package aufgabe7;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        SearchAlgos bs = new SearchAlgos();
        bs.fixData();

        Scanner scan=new Scanner(System.in);
        String s="";

        System.out.println("Welche Postleitzahl möchten Sie suchen?");

        while(true){
            s=scan.nextLine();
            if(s.matches("\\d+")) {
                bs.binarySearch(Integer.parseInt(s));
                bs.interpolSearch(Integer.parseInt(bs.getAlhp().get(1).getPc()),Integer.parseInt(bs.getAlhp().get(bs.getAlhp().size()-1).getPc()),Integer.parseInt(s));
                bs.printEffort();
            } else {
                System.out.println("Das ist keine Zahl.");
            }
        }
    }
}
