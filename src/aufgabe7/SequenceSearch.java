package aufgabe7;

import aufgabe6.Hospital;
import aufgabe6.ReadWrite;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class SequenceSearch {

    ArrayList<Hospital> alhp = new ArrayList<Hospital>();
    int successful=0;
    int unsuccessful=0;

    public void fixData() {
        String[] data;
        BufferedReader br = null;
        String s = "";

        try {
            br = new BufferedReader(new FileReader("C://Users//ButtStallion//eclipse-workspace//AlgoDat//src//aufgabe7//KHR95_red.txt"));
            while ((s = br.readLine()) != null) {
                if (s.isEmpty() == false) {
                    data = (s.split("\t"));
                    write(data[0], data[1], data[2], data[3], data[4]);
                }
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public ArrayList<Hospital> getAlhp() {
        return alhp;
    }

    private void write(String _name, String _street, String _pc, String _city, String _beds) {
        alhp.add(new Hospital(_name, _street, _pc, _city, _beds));
    }

    public void sequentialSearch(int _num) {
        int index = 0;

        while (index < alhp.size() && Integer.parseInt(alhp.get(index).getPc()) != _num) {
            index++;
        }
        if (index == alhp.size()) {
            System.out.println("Nicht gefunden.");
            unsuccessful++;
        } else {
            System.out.println("Position: " + index);
            successful++;
        }
    }

    public void randomSuccessfulSearch(){
        for(int i=1; i<=1000;i++){
            sequentialSearch(Integer.parseInt(alhp.get((int)(Math.random()*alhp.size())).getPc()));
        }
    }

    public void randomUnsuccessfulSearch(){
        for(int i=1; i<=1000;i++){
            sequentialSearch(i);
        }
    }

    public void successrate(){
        System.out.println("Aufwand für erfolgreich: " + successful + "\nAufwand für erfolglos: " + unsuccessful);
    }
}
