package aufgabe7;

import aufgabe6.Hospital;
import aufgabe6.ReadWrite;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class SearchAlgos {
    private ArrayList<Hospital> alhp = new ArrayList<Hospital>();
    private int bs=0;
    private int is=0;

    public void fixData() {
        String[] data;
        BufferedReader br = null;
        String s = "";

        try {
            br = new BufferedReader(new FileReader("C://Users//ButtStallion//eclipse-workspace//AlgoDat//src//aufgabe7//stichprobe"));
            while ((s = br.readLine()) != null) {
                if (s.isEmpty() == false) {
                    data = (s.split("\t"));
                    write(data[0], data[1], data[2], data[3], data[4]);
                    selectionSort();
                }
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public ArrayList<Hospital> getAlhp() {
        return alhp;
    }

    private void write(String _name, String _street, String _pc, String _city, String _beds) {
        alhp.add(new Hospital(_name, _street, _pc, _city, _beds));
    }

    public void binarySearch(int _num) {
        int mid = 0;

        int left = 1;
        int right = alhp.size() - 1;

        do {
            bs++;
            mid = (left + right) / 2;

            if (Integer.parseInt(alhp.get(mid).getPc()) < _num) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        } while (Integer.parseInt(alhp.get(mid).getPc()) != _num && left <= right);

        if (Integer.parseInt(alhp.get(mid).getPc()) == _num) {
            System.out.println("Position: " + mid);
        } else {
            System.out.println("Nicht gefunden.");
        }
    }

    public void selectionSort() {
        for (int x = 1; x < alhp.size() - 1; x++) {
            for (int y = x + 1; y < alhp.size(); y++) {
                Hospital hx = alhp.get(x);
                Hospital hy = alhp.get(y);

                if (Integer.parseInt(hx.getPc()) > Integer.parseInt(hy.getPc())) {
                    Hospital help = alhp.get(x);
                    alhp.set(x, alhp.get(y));
                    alhp.set(y, help);
                }
            }
        }
    }

    public void interpolSearch(int _start, int _end, int _num) {
        int endIndex = alhp.size() - 1;
        int startIndex = 1;
        double limit = startIndex + ((double) (_num - _start) / (double) (_end - _start)) * (endIndex - startIndex);

        if ((int) limit >= endIndex) {
            System.out.println("Nicht gefunden.");
            return;
        }

        if (_num > Integer.parseInt(alhp.get((int) limit).getPc())) {
            interpolSearch((int) (limit + 1), _end, _num);
            is++;
        } else if (_num < Integer.parseInt(alhp.get((int) limit).getPc()) && _start != limit) {
            interpolSearch(_start, (int) (limit - 1), _num);
            is++;
        } else if (_num == Integer.parseInt(alhp.get((int) limit).getPc())) {
            System.out.println("Position: " + (int) limit);
            is++;
        } else {
            System.out.println("Nicht gefunden.");
        }
    }

    public void printEffort(){
        System.out.println("Mittlerer Aufwand: " + (bs+is)/2);
    }
}
