package aufgabe4punkt1;

//import com.sun.org.apache.bcel.internal.generic.NEW;
//import com.sun.org.apache.xpath.internal.compiler.Keywords;

public class Element {


    private char value;
    private Element right, left, root;
    private  boolean set;

    public Element(char _value,Element _root) {

        this.value = _value;
        right = null;
        left = null;
        root = _root;
        set = false;
    }

    public boolean isset(){
        return this.set;
    }
   public void setnode(){
        this.set= true;
   }

    public char getValue() {
        return this.value;
    }

    public void setRight(Element _right) {
        this.right = _right;
    }

    public void setLeft(Element _left) {
        this.left = _left;
    }

    public Element getRight() {
        return this.right;
    }

    public void setroot(Element _root) {
        this.root = _root;
    }

    public Element getroot() {
        return this.root;
    }

    public Element getLeft() {
        return this.left;
    }

    public boolean hasRight() {
        if (this.getRight() != null)
            return true;
        else
            return false;
    }

    public boolean hasRoot() {
        if (this.getroot() != null)
            return true;
        else
            return false;
    }

    public boolean hasLeft() {
        if (this.getLeft() != null)
            return true;
        else
            return false;
    }

}