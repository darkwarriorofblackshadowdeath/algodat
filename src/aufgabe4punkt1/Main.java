package aufgabe4punkt1;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        String sentence = "Dies_ist_ein_benaerer_Baum_zum_traversieren.";
        Element root = new Element(sentence.charAt(0), null);
        buildSentence(selectWords(sentence), root);
//        printTree(root);
//        System.out.println("inorder: ");
//      inorder(inorderStart(root));
//        System.out.println("preorder: ");
//        preorder(root);
        postorder(root);
        //       inorder(root);
        //       levelorder(root);


    }

    static public void addWord(Element node, String word) {
        for (int i = 0; i < word.length(); i++) {
            node.setRight(new Element(word.charAt(i), node));
            if (i < word.length()) {
                node = node.getRight();
            }
        }
    }

    static public void buildSentence(String[] sentence, Element node) {
        int j = 0;

        while (sentence[j] != " ") {

            if (node.hasRight()) {
                node.setLeft(new Element(sentence[j].charAt(0), node));
                node = node.getLeft();
                j++;
            }

            addWord(node, sentence[j]);
            j++;
        }

    }

    static public String[] selectWords(String sentence) {
        String[] words = new String[3000];

        for (int i = 0; i < words.length; i++) {
            words[i] = " ";
        }
        String word = " ";
        int index = 0;
        int count = 0;

        word = sentence.substring(index + 1, sentence.indexOf("_"));
        words[count] = word;
        count++;
        index = sentence.indexOf("_");
        words[count] = Character.toString(sentence.charAt(index));
        sentence = sentence.substring(index + 1);
        index = 0;
        count++;

        while (sentence.contains("_")) {
            word = sentence.substring(index, sentence.indexOf("_"));
            words[count] = word;
            count++;
            index = sentence.indexOf("_");
            words[count] = Character.toString(sentence.charAt(index));
            sentence = sentence.substring(index + 1);
            index = 0;
            count++;
        }
        word = sentence.substring(index, sentence.indexOf("."));
        words[count] = word;
        count++;
        index = sentence.indexOf(".");
        words[count] = Character.toString(sentence.charAt(index));
        sentence = sentence.substring(index + 1);
        index = 0;
        count++;

        return words;
    }

    static public void printTree(Element node) {
        System.out.println("print tree:");
        System.out.println("root: " + node.getValue());
        Element root = node;
        do {
            System.out.println("right: ");
            while (node.hasRight()) {
                node = node.getRight();
                System.out.println(node.getValue());
            }
            root = root.getLeft();
            node = root;
            System.out.println("left: ");
            System.out.println(node.getValue());
        } while (node.hasLeft());
    }

    static public Element inorderStart(Element node) {
        Element start = node;
        do {
            start = start.getLeft();
        }
        while (start.hasLeft());
        return start;
    }

    static public void inorder(Element node) {
        if (node.isset() == false) {
            node.setnode();
            System.out.print(node.getValue());
        }
        if (node.hasLeft()) {
            if (node.getLeft().isset() == false) {
                System.out.print(node.getLeft().getValue());
                node.getLeft().setnode();
                inorder(node.getLeft());
            }
        }
        if (node.hasRoot()) {
            if (node.getroot().isset() == false) {
                System.out.print(node.getroot().getValue());
                node.getroot().setnode();
                inorder(node.getroot().getRight());
                inorder(node.getroot());
            }
        }
        if (node.hasRight()) {
            if (node.getRight().isset() == false) {
                System.out.print(node.getRight().getValue());
                node.getRight().setnode();
                inorder(node.getRight());
            }
        }
    }

    static void preorder(Element node) {
        if (node.isset() == false) {
            node.setnode();
            System.out.print(node.getValue());
        }
        if (node.hasLeft()) {
            if (node.getLeft().isset() == false) {
                System.out.print(node.getLeft().getValue());
                node.getLeft().setnode();
                inorder(node.getLeft());
            }
        }
        if (node.hasRight()) {
            if (node.getRight().isset() == false) {
                System.out.print(node.getRight().getValue());
                node.getRight().setnode();
                inorder(node.getRight());
            }
        }
    }

    static public void postorder(Element node) {
        if (node.hasLeft()) {
            postorder(node.getLeft());
        }

        if (node.hasRight()){
            postorder(node.getRight());
        }
            System.out.print(node.getValue());
    }


    static public void levelorder(Element node) {
        ArrayList<Element> tree = new ArrayList<>();
        int nodeindex = 0;
        tree.add(node);
        while (nodeindex < tree.size()) {
            if (tree.get(nodeindex).hasLeft()) {
                tree.add(tree.get(nodeindex).getLeft());
            }
            if (tree.get(nodeindex).hasRight()) {
                tree.add(tree.get(nodeindex).getRight());
            }
            nodeindex++;
        }
        for (Element n : tree)
            System.out.print(n.getValue());
    }
}







