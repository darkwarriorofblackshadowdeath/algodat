package aufgabe3;

import java.util.ArrayList;

public class Element {
	Element rightElement = null;
	Element leftElement = null;
	public Character value;

	public void setValue(char _value) {
		this.value = _value;
	}

	public void traverseInOrder() {
		if (this.leftElement != null)
			this.leftElement.traverseInOrder();
		System.out.print(this.value);
		if (this.rightElement != null)
			this.rightElement.traverseInOrder();
	}

	public void traversePreOrder() {
		System.out.print(this.value);
		if (this.leftElement != null)
			this.leftElement.traversePreOrder();
		if (this.rightElement != null)
			this.rightElement.traversePreOrder();
	}

	public void traversePostOrder() {
		if (this.leftElement != null)
			this.leftElement.traversePostOrder();
		if (this.rightElement != null)
			this.rightElement.traversePostOrder();
		System.out.print(this.value);
	}

	public void traverseLevelOrder() {
		ArrayList<Element> tree = new ArrayList<>();
		int elementIterator = 0;
		tree.add(this);
		while (elementIterator < tree.size()) {
			if (tree.get(elementIterator).leftElement != null)
				tree.add(tree.get(elementIterator).leftElement);
			if (tree.get(elementIterator).rightElement != null)
				tree.add(tree.get(elementIterator).rightElement);
			elementIterator++;
		}
		for (Element node : tree)
			System.out.print(node.value);
	}
}
