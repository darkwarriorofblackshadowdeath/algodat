package aufgabe3;

public class List {
	Element startElement = new Element();

	public void appendElement(char charAt) {
		Element tempElement = this.startElement;
		if (tempElement.value == null) {
			tempElement.setValue(charAt);
		} else {
			while (tempElement.leftElement != null)
				tempElement = tempElement.leftElement;
			if (charAt == ' ' || charAt == '.') {
				tempElement.leftElement = new Element();
				tempElement = tempElement.leftElement;
				tempElement.setValue(charAt);
			} else {
				if (tempElement.rightElement == null) {
					tempElement.rightElement = new Element();
					tempElement.rightElement.setValue(charAt);
				} else {
					tempElement = tempElement.rightElement;
					while (tempElement.rightElement != null) {
						tempElement = tempElement.rightElement;
					}
					tempElement.rightElement = new Element();
					tempElement.rightElement.setValue(charAt);
				}
			}
		}
	}
	
	public void appendString(String _inputString) {
		while (_inputString.length() != 0) {
			this.appendElement(_inputString.charAt(0));
			_inputString = _inputString.substring(1);
		}		
	}
	
	public void traverse() {
		System.out.println(System.lineSeparator());
		System.out.println("###### INORDER ######");
		this.startElement.traverseInOrder();
		System.out.println(System.lineSeparator());
		System.out.println("###### PREORDER ######");
		this.startElement.traversePreOrder();
		System.out.println(System.lineSeparator());
		System.out.println("###### POSTORDER ######");
		this.startElement.traversePostOrder();
		System.out.println(System.lineSeparator());
		System.out.println("###### LEVELORDER ######");
		this.startElement.traverseLevelOrder();
	}
	

	
	
	
}
