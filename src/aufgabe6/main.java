package aufgabe6;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        ReadWrite rw = new ReadWrite();
        Scanner scan = new Scanner(System.in);
        rw.fixData();
        System.out.println("Möchten Sie lesen (l) oder schreiben (s)?");
        while (true) {
            String s = scan.nextLine();
            if (s.equals("l")) {
//                rw.selectionSort();
                rw.bubbleSort();
                rw.read();
            } else if (s.equals("s")) {
                System.out.println("Bitte trennen Sie Ihre Eingabe mit einem Komma (Name,Straße,Postleitzahl,Ort,Betten).");
                Scanner entry = new Scanner(System.in);
                String en = entry.nextLine();
                String[] a = en.split("\\,");
                if (a.length == 5 && a[2].matches("\\d+")) {
                    rw.write(a[0], a[1], a[2], a[3], a[4]);
                    System.out.println("Krankenhaus hinzugefügt!");
                    System.out.println("Möchten Sie lesen (l) oder schreiben (s)?");
                } else {
                    System.out.println("Die Eingabe ist nicht korrekt.");
                    System.out.println("Möchten Sie lesen (l) oder schreiben (s)?");
                }
            } else {
                System.out.println("Möchten Sie die Anwendung Schließen?(j/n)");
                if (s.equals("j")) {
                    scan.close();
                } else if (s.equals("n")) {
                    System.out.println("Möchten Sie lesen (l) oder schreiben (s)?");
                }
            }
        }
    }
}
