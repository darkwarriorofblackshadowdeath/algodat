package aufgabe6;

public class Hospital {

    private String name, street, pc, city, beds; //pc=postal code

    public Hospital(String _name, String _street, String _pc, String _city, String _beds) {
        this.name = _name;
        this.street = _street;
        this.pc = _pc;
        this.city = _city;
        this.beds = _beds;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getPc() { return pc; }

    public String getCity() { return city; }

    public String getBeds() {
        return beds;
    }
}
