/**
 *
 */
package aufgabe6;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Anna Spillner
 */
public class ReadWrite {
    private ArrayList<Hospital> alhp = new ArrayList<Hospital>();

    public void fixData() {
        String[] data;
        BufferedReader br = null;
        String s = "";

        try {
            br = new BufferedReader(new FileReader("C://Users//ButtStallion//eclipse-workspace//AlgoDat//src//aufgabe6//KHR95_red.txt"));
//            br = new BufferedReader(new FileReader("C://Users//ButtStallion//eclipse-workspace//AlgoDat//src//aufgabe6//datensatz"));
            while ((s = br.readLine()) != null) {
                if (s.isEmpty() == false) {
                    data = (s.split("\t"));
                    write(data[0], data[1], data[2], data[3], data[4]);
                }
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void read() {
        for (int i = 0; i <= alhp.size() - 1; i++) {
            System.out.println(alhp.get(i).getName() + " " + alhp.get(i).getStreet() + " " + alhp.get(i).getPc() + " " + alhp.get(i).getCity() + " " + alhp.get(i).getBeds());
        }
    }

    public void write(String _name, String _street, String _pc, String _city, String _beds) {
        alhp.add(new Hospital(_name, _street, _pc, _city, _beds));
    }

    public void selectionSort() {
        int counter = 0;
        for (int x = 1; x < alhp.size() - 1; x++) {
            for (int y = x + 1; y < alhp.size(); y++) {
                if (Integer.parseInt(alhp.get(x).getPc()) > Integer.parseInt(alhp.get(y).getPc())) {
                    Hospital help = alhp.get(x);
                    alhp.set(x, alhp.get(y));
                    alhp.set(y, help);
                    counter++;
                }
            }
        }
        System.out.println("Selectionsort: " + counter);
    }

    public void bubbleSort() {
        boolean flag = true;
        int counter = 0;

        while (flag) {
            flag = false;
            for (int i = 1; i < alhp.size() - 1; i++) {
                counter++;

                if (alhp.get(i).getCity().compareToIgnoreCase(alhp.get(i + 1).getCity()) > 0) {
                    flag = true;
                    Hospital help = alhp.get(i);
                    alhp.set(i, alhp.get(i + 1));
                    alhp.set(i + 1, help);
                }
            }
        }
        System.out.println("Bubblesort: " + counter);
    }
}
