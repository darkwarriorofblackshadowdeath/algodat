package aufgabe2punkt1;

import java.io.IOException;
import java.io.InputStreamReader;

public class Spooler {

    public static void main(String[] args) {
        char[] snake = new char[20];
        int index = 0;
        int latest = 0;
        boolean isFull = false;
        boolean isPaused = false;
        while (true) {
            try {
                char tempChar = (char) new InputStreamReader(System.in).read();
                if (tempChar == '/')
                    isPaused = true;
                if (tempChar == '*')
                    isPaused = false;
                if (tempChar == '-') {
                    while (index < latest && index + 20 - latest > 0 && index != latest) {
                        System.out.println(snake[latest]);
                        latest++;
                        if (latest == 20)
                            latest = 0;
                    }
                    while (index - latest > 0) {
                        System.out.println(snake[latest]);
                        latest++;
                    }
                    System.exit(0);
                }
                if (!isFull && tempChar != '+' && tempChar != '#' && tempChar != '-') {
                    snake[index] = tempChar;
                    index++;
                }
                if (index == 20)
                    index = 0;
                if (index - latest > 19 && tempChar != '-') {
                    isFull = true;
                    System.out.println("Voll.");
                } else if (index <= latest && index + 20 - latest > 19 && tempChar != '-' && tempChar != '*' && tempChar != '/') {
                    isFull = true;
                    System.out.println("Voll.");
                } else
                    isFull = false;
                while (index - latest > 10 && !isPaused) {
                    System.out.println(snake[latest]);
                    latest++;
                }
                while (index <= latest && index + 20 - latest > 10 && !isPaused) {
                    System.out.println(snake[latest]);
                    latest++;
                    if (latest == 20)
                        latest = 0;
                }

            } catch (IOException e) {

                e.printStackTrace();
            }
        }
    }

}

