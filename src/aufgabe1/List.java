package aufgabe1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class List implements Serializable {
	public Element startElement = new Element();

	public void append(String _vorname, String _nachname, String _geburtsdatum, String _versNummer) {
		this.getLastElement().insert(new Element().set_vorname(_vorname).set_nachname(_nachname)
				.set_geburtsdatum(_geburtsdatum).set_versNummer(_versNummer));
	}

	public static List loadList(String _fileName) throws FileNotFoundException {
		ObjectInput fin;
		List tempList = new List();
		Element temp = null;
		File datei = new File(_fileName);
		if (!datei.exists() || datei.isDirectory())
			throw new FileNotFoundException("Die Datei wurde nicht gefunden.");

		try {
			fin = new ObjectInputStream(new FileInputStream(_fileName));

			tempList = (List) fin.readObject();
				
			
			fin.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return tempList;
	}

	

	/**
	 * legt eine Liste persistent ab
	 * 
	 * @throws IOException
	 */
	public void saveList() throws IOException {
		ObjectOutput fout = new ObjectOutputStream(new FileOutputStream("savedList.ser"));

		fout.writeObject(this);
		fout.close();
	}

	/**
	 * Diese Methode gitb die Anzahl der Elemente einer Liste zurück.
	 * 
	 * @return
	 */
	public int countElements() {
		Element tempElement = this.startElement;
		int counter = 0;
		while (tempElement.nextElement != null) {
			tempElement = tempElement.nextElement;
			counter++;
		}
		return counter;
	}

	/**
	 * Diese Methode gibt das Element mit der passenden verNummer zurück, sollte
	 * diese in der Liste nicht gefunden werden, wird null zurückgegeben
	 * 
	 * @param _versNummer
	 * @return Element
	 */
	// TODO
	public Element fintElementbyVersNummer(String _versNummer) {
		Element tempElement = this.startElement;
		if (tempElement.nextElement == null) {
			System.out.println("Die Liste ist leer.");
			return null;
		}
		tempElement = tempElement.get_nextElement();
		while (tempElement.nextElement != null && !tempElement.get_versNummer().equals(_versNummer))
			tempElement = tempElement.get_nextElement();
		if (!tempElement.get_versNummer().equals(_versNummer)) {
			System.out.println("Die Versicherungsnummer konnte nicht gefunden werden.");
			return null;
		}
		return tempElement;
	}

	/**
	 * Diese Methode gibt das letzte Element einer Liste zurück.
	 * 
	 * @return Element
	 */
	public Element getLastElement() {
		Element tempElement = startElement;
		if (tempElement.nextElement == null)
			return tempElement;
		while (tempElement.nextElement != null)
			tempElement = tempElement.nextElement;
		return tempElement;
	}

	public void printList() {
		Element tempElement = this.startElement;
		if (tempElement.nextElement == null)
			System.out.println("Die Liste ist leer.");
		while (tempElement.nextElement != null) {
			tempElement = tempElement.nextElement;
			tempElement.printElement();
		}
	}

}
