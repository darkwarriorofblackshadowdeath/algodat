package aufgabe1;

import java.io.Serializable;

public class Element implements Serializable {
	Element nextElement = null;
	Element prevElement = null;
	String vorname;
	String nachname;
	String geburtsdatum;
	String versNummer;

	/**
	 * Diese Methode entfernt das Element aus der Liste
	 */
	public void deleteElement() {
		if (this.nextElement == null) {
			this.get_prevElement().set_nextElement(null);
		} else {
			this.get_prevElement().set_nextElement(this.get_nextElement());
		
		this.get_nextElement().set_prevElement(this.get_prevElement());
		}
	}

	/**
	 * Diese Methode erzeugt ein neues Element hinter dem Element, auf dem sie
	 * angewendet wird.
	 * 
	 * @param _vorname
	 * @param _nachname
	 * @param _geburtsdatum
	 * @param _versNummer
	 */
	public void insert(Element _elementToInsert) {
		if (this.nextElement != null)
			_elementToInsert.set_nextElement(this.get_nextElement());
		_elementToInsert.set_prevElement(this);
		this.set_nextElement(_elementToInsert);
	}

	@Override
	public String toString() {
		return "Vorname: " + this.get_vorname() + System.lineSeparator() + "Nachname: " + this.get_nachname()
				+ System.lineSeparator() + "Geburtsdatum: " + this.get_geburtsdatum() + System.lineSeparator()
				+ "Versicherungsnummer: " + this.get_versNummer() + System.lineSeparator();
	}

	public void printElement() {
		System.out.println(this.toString());
	}

	/**
	 * @return the _nextElement
	 */
	public Element get_nextElement() {
		return nextElement;
	}

	/**
	 * @param _nextElement
	 *            the _nextElement to set
	 * @return
	 */
	public Element set_nextElement(Element _nextElement) {
		this.nextElement = _nextElement;
		return this;
	}

	/**
	 * @return the _prevElement
	 */
	public Element get_prevElement() {
		return prevElement;
	}

	/**
	 * @param _prevElement
	 *            the _prevElement to set
	 * @return
	 */
	public Element set_prevElement(Element _prevElement) {
		this.prevElement = _prevElement;
		return this;
	}

	/**
	 * @return the _vorname
	 */
	public String get_vorname() {
		return vorname;
	}

	/**
	 * @param _vorname
	 *            the _vorname to set
	 */
	public Element set_vorname(String _vorname) {
		this.vorname = _vorname;
		return this;
	}

	/**
	 * @return the _nachname
	 */
	public String get_nachname() {
		return nachname;
	}

	/**
	 * @param _nachname
	 *            the _nachname to set
	 * @return
	 */
	public Element set_nachname(String _nachname) {
		this.nachname = _nachname;
		return this;
	}

	/**
	 * @return the _geburtsdatum
	 */
	public String get_geburtsdatum() {
		return geburtsdatum;
	}

	/**
	 * @param _geburtsdatum
	 *            the _geburtsdatum to set
	 * @return
	 */
	public Element set_geburtsdatum(String _geburtsdatum) {
		this.geburtsdatum = _geburtsdatum;
		return this;
	}

	/**
	 * @return the _versNummer
	 */
	public String get_versNummer() {
		return versNummer;
	}

	/**
	 * @param _versNummer
	 *            the _versNummer to set
	 * @return
	 */
	public Element set_versNummer(String _versNummer) {
		this.versNummer = _versNummer;
		return this;
	}

}
