package aufgabe1;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Start {

	public static void main(String[] args) {
		List liste = new List();
		try {
			liste = List.loadList("savedList.ser");
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		liste.append("Robert", "Pfeiffer", "20.05.1985", "200585p12726");//zu löschen
		liste.append("Max", "Ausgemustertmann", "01.01.0001", "010101a12725");
		liste.append("Robert1", "Pfeiffer", "20.05.1985", "200585p12727");//zu löschen
		liste.append("Max1", "Ausgemustertmann", "01.01.0001", "010101a12728");
		liste.append("Robert2", "Pfeiffer", "20.05.1985", "200585p12720");
		liste.append("Max2", "Ausgemustertmann", "01.01.0001", "010101a12729");//zu löschen
	
		liste.getLastElement().printElement();
		liste.printList();
		liste.fintElementbyVersNummer("200585p12726").deleteElement();
		liste.fintElementbyVersNummer("200585p12727").deleteElement();
		liste.fintElementbyVersNummer("010101a12729").deleteElement();
		
		liste.fintElementbyVersNummer("010101a12725").printElement();
		System.out.println(liste.countElements());
		try {
			liste.saveList();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List liste1 = new List();
		
		try {
			liste1 = List.loadList("savedList.ser");
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		liste1.printList();
		
	}
}
