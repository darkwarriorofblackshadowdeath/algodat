package aufgabe2punkt2;

public class Euklid {
private static int recurvCounter =0;
	public static void main(String[] args) {
		System.out.println("ggT:"+euklid(1071, 1029));
		System.out.println("Rekursionen: "+recurvCounter);
	}

	public static int euklid(int a, int b) {
		if (b == 0) {
			return a;
		} else if (a == 0) {
			return b;
		} else if (a > b) {
			recurvCounter++;
			return euklid(a - b, b);			
		} else {
			recurvCounter++;
			return euklid(a, b - a);
		}
	}
}