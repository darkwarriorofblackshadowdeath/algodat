package aufgabe5punkt2;

public class Element {


    private String value;
    private Element right, left, root;

    public Element(String _value,Element root) {

        this.value = _value;
        right = null;
        left = null;
        this.root = root;
    }

    public Element getRoot() {
        return root;
    }


    public String getValue() {
        return this.value;
    }

    public void setRight(Element _right) {
        this.right = _right;
    }

    public void setLeft(Element _left) {
        this.left = _left;
    }

    public Element getRight() {
        return this.right;
    }

    public Element getLeft() {
        return this.left;
    }

    public boolean hasRight() {
        if (this.getRight() != null)
            return true;
        else
            return false;
    }


    public boolean hasLeft() {
        if (this.getLeft() != null)
            return true;
        else
            return false;
    }
    public boolean hasroot() {
        if (this.getRoot() != null)
            return true;
        else
            return false;
    }

}