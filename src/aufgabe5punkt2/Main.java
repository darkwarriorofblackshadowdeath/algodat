package aufgabe5punkt2;

public class Main {
    public static void main(String[] args) {
        String[] Names = {"alexander", "david", "felix", "maximilian",
                "leon", "lucas ", "luca", "paul", "jonas", "tim",
                "anna", "emily", "julia", "maria", "laura", "lea", "lena", "leonie", "marie", "sophie"};
        String[] hashnames = deletespace(hash(Names));
        Element root = buildtree(Names);
        Element hashroot = buildtree(hashnames);
        System.out.println("inorder normal tree:  \n");
        inorder(root);
        System.out.println(" ");
        System.out.println("inorder hash tree:  \n");
        inorder(hashroot);
        System.out.println(" ");
        System.out.println("höhe normal tree:  \n");
        System.out.println((height(root)));
        System.out.println("höhe hash tree:  \n");
        System.out.println((height(hashroot)));
    }

    public static String[] hash(String[] names) {
        int speicherort = 0;
        String[] speicher = new String[32];
        int kollisionsum = 0;

        for (int j = 0; j < 32; j++) {
            speicher[j] = " ";
        }
        for (int i = 0; i < names.length; i++) {
            int kollision = 0;
            speicherort = names[i].charAt(0) + names[i].charAt(1) + names[i].charAt(2);
            speicherort = speicherort % 32;
            if (speicher[speicherort] == " ") {
                speicher[speicherort] = names[i];
            } else {
                while (speicher[speicherort] != " ") {
                    speicherort++;
                    kollision++;
                    if (speicherort == 32) {
                        speicherort = 0;
                    }
                }
                speicher[speicherort] = names[i];
                kollisionsum = kollisionsum + kollision;
            }
        }
        return speicher;
    }

    static public String[] deletespace(String[] names) {
        String[] editnames = new String[20];
        int index = 0;
        for (int i = 0; i < names.length; i++) {
            if (names[i] != " ") {
                editnames[index] = names[i];
                index++;
            }
        }
        return editnames;
    }

    static public Element buildtree(String[] names) {
        Element root = new Element(names[0], null);
        for (int i = 1; i < names.length; i++) {
            if (root.getValue().compareToIgnoreCase(names[i]) > 0) {
                if (!root.hasRight()) {
                    root.setRight(new Element(names[i], root));
                } else {
                    Element node = root;
                    while (node.hasRight() || node.hasLeft()) {
                        if (node.hasLeft()) {
                            node = node.getLeft();
                        } else {
                            node = node.getRight();
                        }
                        if (node.getValue().compareToIgnoreCase(names[i]) > 0) {
                            if (!node.hasRight()) {
                                node.setRight(new Element(names[i], node));
                                break;
                            }

                        } else {
                            if (!node.hasLeft()) {
                                node.setLeft(new Element(names[i], node));
                                break;
                            }

                        }
                    }
                }
            } else {
                if (!root.hasLeft()) {
                    root.setLeft(new Element(names[i], root));
                } else {

                    Element node = root;
                    while (node.hasRight() || node.hasLeft()) {
                        if (node.hasLeft()) {
                            node = node.getLeft();
                        } else {
                            node = node.getRight();
                        }
                        if (node.getValue().compareToIgnoreCase(names[i]) > 0) {
                            if (!node.hasRight()) {
                                node.setRight(new Element(names[i], node));
                                break;
                            }
                        } else {
                            if (!node.hasLeft()) {
                                node.setLeft(new Element(names[i], node));
                                break;
                            }

                        }
                    }
                }
            }
        }

        return root;
    }

    static public void inorder(Element root) {
        if (root.hasLeft()) {
            inorder(root.getLeft());
        }
        System.out.print(root.getValue() + " ");
        if (root.hasRight()) {
            inorder(root.getRight());
        }
    }

    static int height(Element root) {
        if (root == null)
            return 0;
        else {
            return 1 + add(height(root.getLeft()), height(root.getRight()));
        }
    }

    static public int add(int leftHeight, int rightHeight) {
        if (leftHeight < rightHeight)
            return rightHeight;

        else
            return leftHeight;
    }
}
