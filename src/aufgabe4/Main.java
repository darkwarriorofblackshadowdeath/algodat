package aufgabe4;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        String sentence = "Dies_ist_ein_benaerer_Baum_zum_traversieren.";
        Element root = new Element(sentence.charAt(0), null);
        buildSentence(selectWords(sentence), root);
        countnode(root);
        height(root);

    }

    static public void addWord(Element node, String word) {
        for (int i = 0; i < word.length(); i++) {
            node.setRight(new Element(word.charAt(i), node));
            if (i < word.length()) {
                node = node.getRight();
            }
        }
    }

    static public void buildSentence(ArrayList<String> sentence, Element node) {
        int j = 0;

        while (sentence.get(j) != " ") {

            if (node.hasRight()) {
                node.setLeft(new Element(sentence.get(j).charAt(0), node));
                node = node.getLeft();
                j++;
            }

            if (sentence.get(j) != " ") {
                addWord(node, sentence.get(j));
                j++;
            }
        }

    }

    static public ArrayList<String> selectWords(String sentence) {
        ArrayList<String> words = new ArrayList<String>();

//        for (int i = 0; i < words.length; i++) {
//            words[i] = " ";
//        }
        String word = " ";
        int index = 0;
        int count = 0;

        word = sentence.substring(index + 1, sentence.indexOf("_"));
        words.set(count,word);
        count++;
        index = sentence.indexOf("_");
        words.set(count, Character.toString(sentence.charAt(index)));
        sentence = sentence.substring(index + 1);
        index = 0;
        count++;

        while (sentence.contains("_")) {
            word = sentence.substring(index, sentence.indexOf("_"));
            words.set(count,word);
            count++;
            index = sentence.indexOf("_");
            words.set(count, Character.toString(sentence.charAt(index)));
            sentence = sentence.substring(index + 1);
            index = 0;
            count++;
        }
        word = sentence.substring(index, sentence.indexOf("."));
        words.set(count,word);
        count++;
        index = sentence.indexOf(".");
        words.set(count, Character.toString(sentence.charAt(index)));
        sentence = sentence.substring(index + 1);
        index = 0;
        count++;

        return words;
    }

    public static void countnode (Element node){
        ArrayList<Element> tree = new ArrayList<>();
        int nodeindex = 0;
        int maxpath =0;
        int countendnotes =0;
        int edges =0;
        char[] endnodes= new char[300];
        tree.add(node);
        while (nodeindex < tree.size()) {
            if (tree.get(nodeindex).hasLeft()) {
                tree.add(tree.get(nodeindex).getLeft());
                edges++;
            }else{
                if(tree.get(nodeindex).hasRight()== false) {
                    endnodes[countendnotes] = tree.get(nodeindex).getValue();
                    countendnotes++;
                }
            }
            if (tree.get(nodeindex).hasRight()) {
                tree.add(tree.get(nodeindex).getRight());
                edges++;
            }

            nodeindex++;
            maxpath=maxpath+nodeindex;
        }
        System.out.println("Knotenzahl: " + tree.size());
        System.out.println("davon Blätter: " + countendnotes);
        for (int i =0; i<countendnotes;i++){
            System.out.print(i + ": "+ endnodes[i]+ " ");
        }
        int innernode = tree.size()-(countendnotes+1);
        System.out.println("innere Knoten: " + innernode);
        System.out.println("Kanten: " + edges);
        System.out.println("maximaler Pfad ist: " + maxpath);
    }
    public static void height(Element node){
        Element start = node;
        int[]left = new int[40];
        int[]reight = new int[40];
        int index = 0;
        for (int i = 0; i<40;i++){
            left[i]=0;
            reight[i]=0;
        }
        while (start.hasLeft()){
            if (start.hasRight()) {
                Element r =start;
                while (r.hasRight()) {
                    reight[index]++;
                    r= r.getRight();
                }
            }
            left[index]= 1+index;
            index++;
            start=start.getLeft();
        }
        int height =left[0]+reight[0];
        for (int i=1; i<index+1;i++){
            if (height< (left[i]+reight[i])){
                height = left[i]+reight[i];
            }
        }
        System.out.println("Höhe: "+height);
        int pathlenght =0;
        for (int i=1;i<height+1;i++){
            pathlenght+= i;
        }
        System.out.println("Pfadlänge: "+pathlenght);
    }

}
